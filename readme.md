```
Nama    : Arnoldy Fatwa Rahmadin

NIM     : 222011330

Kelas   : 3SI3
```

## Praktikum Pemrograman Platform Khusus Pertemuan 13

1. Dokumentasi Praktikum

    - Antarmuka 1
        ![ss1](dokumentasi/ss1.jpg)
    - Antarmuka 2
        ![ss2](dokumentasi/ss2.jpg)
    - Antarmuka 3
        ![ss3](dokumentasi/ss3.jpg)

2. Mengubah kode menggunakan kelas "java.util.concurrent" saja

    - Antarmuka 1
        ![ss1](dokumentasi/ss4.jpg)
    - Antarmuka 2
        ![ss2](dokumentasi/ss5.jpg)
    - Antarmuka 3
        ![ss3](dokumentasi/ss6.jpg)